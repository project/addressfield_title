<?php
/**
 * Returns form for changing configuration options
 *
 * @param $form
 *   form
 * @param $form_state
 *   form state
 *
 * @return
 *   configuration form
 */
function addressfield_title_admin_form($form, &$form_state) {
  $form = array();
  $form['addressfield_title_prefix'] = array(
    '#type' => 'textarea',
    '#title' => t('Title values'),
    '#description' => t('The possible prefixes that can be used. Enter one value per line, do not include the leading dot.'),
    '#default_value' => variable_get("addressfield_title_prefix", "Miss\r\nMrs\r\nMs\r\nMr\r\nDr\r\nProf\r\nRev\r\nLady\r\nLord\r\nSir"),
  );
  $form['addressfield_title_form_element'] = array(
    '#type' => 'select',
    '#title' => t('Form element'),
    '#description' => t('The form element that allows you to choose between the title options.'),
    '#options' => array(
      'select' => t('Select list'),
      'radios' => t('Radio buttons'),
    ),
    '#default_value' => variable_get('addressfield_title_form_element', 'select'),
  );
  return system_settings_form($form);
}
