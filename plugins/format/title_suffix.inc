<?php

/**
 * @file
 * The default format for addresses.
 */

$plugin = array(
  'title' => t('Title suffix'),
  'format callback' => 'addressfield_format_title_suffix_generate',
  'type' => 'title',
  'weight' => -98,
);

/**
 * Format callback.
 *
 * @see CALLBACK_addressfield_format_callback()
 */
function addressfield_format_title_suffix_generate(&$format, $address, $context = array()) {
  if ($context['mode'] == 'form') {
    $format['person_title_suffix'] = array(
      '#type' => 'textfield',
      '#title' => t('Title suffix'),
      '#tag' => 'span',
      '#attributes' => array('class' => array('person-title-suffix')),
      '#default_value' => isset($address['person_title_suffix']) ? $address['person_title_suffix'] : '',
      '#weight' => -195,
    );
  }
  else {
    // Add our own render callback for the format view.
    $format['#pre_render'][] = '_addressfield_title_suffix_render_address';
  }
}
