Extends Address Field[1] by adding a title option (Mr, Mrs, etc). The list of options is hardcoded into the module, I would like to make that configurable in some way.

Based heavily on Jesseh's Address Field Phone module[2].

[1] http://drupal.org/project/addressfield
[2] http://drupal.org/project/addressfield_phone